package ru.mpp.features.sample.ui

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.flowWithLifecycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.mpp.R
import com.example.kmm.application.common.network.KtorClient
import com.example.kmm.application.features.sample.data.TodoApi
import com.example.kmm.application.features.sample.data.TodoApiImpl
import com.example.kmm.application.features.sample.data.TodoDataSource
import com.example.kmm.application.features.sample.data.TodoDataSourceImpl
import com.example.kmm.application.features.sample.data.TodoRepositoryImpl
import com.example.kmm.application.features.sample.domain.GetTodoListUseCase
import com.example.kmm.application.features.sample.domain.GetTodoUseCase
import com.example.kmm.application.features.sample.domain.TodoRepository
import com.example.kmm.application.features.sample.presentation.TodoListViewModel

class MainActivity : AppCompatActivity() {

    //todo replace to di
    private val todoApi: TodoApi = TodoApiImpl(KtorClient)
    private val todoDataSource: TodoDataSource = TodoDataSourceImpl(todoApi)
    private val todoRepository: TodoRepository = TodoRepositoryImpl(todoDataSource)
    private val getTodoListUseCase: GetTodoListUseCase = GetTodoListUseCase(todoRepository)
    private val getTodoUseCase: GetTodoUseCase = GetTodoUseCase(todoRepository)
    private val viewModel = TodoListViewModel(getTodoListUseCase, getTodoUseCase)

    private lateinit var todoIdEditText: EditText
    private lateinit var output: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todoIdEditText = findViewById(R.id.todo_id_edit_text)
        output = findViewById(R.id.text)

        findViewById<Button>(R.id.button).setOnClickListener {
            if (todoIdEditText.text.isNullOrEmpty()) {
                viewModel.updateList()
            } else {
                viewModel.updateTodo(todoIdEditText.text.toString())
            }
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.todoList.observe(lifecycle) {
            output.text = it.joinToString("\n\n\n")
        }

        viewModel.errorText.observe(lifecycle, output::setText)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cancel()
    }
}

fun <T> Flow<T>.observe(lifecycle: Lifecycle, observer: (T) -> Unit) {
    lifecycle.coroutineScope.launch {
        flowWithLifecycle(lifecycle)
            .collect {
                observer(it)
            }
    }
}