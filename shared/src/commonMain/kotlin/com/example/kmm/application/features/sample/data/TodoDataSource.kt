package com.example.kmm.application.features.sample.data

import com.example.kmm.application.features.sample.domain.model.Todo

interface TodoDataSource {

    @Throws(Exception::class)
    suspend fun get(id: Int): Todo

    @Throws(Exception::class)
    suspend fun getAll(): List<Todo>
}

class TodoDataSourceImpl(private val api: TodoApi) : TodoDataSource {

    override suspend fun get(id: Int): Todo =
        api.get(id)

    override suspend fun getAll(): List<Todo> =
        api.getAll()
}