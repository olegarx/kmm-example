package com.example.kmm.application.common.network

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer

val KtorClient = HttpClient {
	install(JsonFeature) {
		serializer = KotlinxSerializer()
	}
}