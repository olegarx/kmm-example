package com.example.kmm.application.features.sample.domain

import com.example.kmm.application.features.sample.domain.model.Todo

class GetTodoListUseCase(private val todoRepository: TodoRepository) {

    @Throws(Exception::class)
    suspend operator fun invoke(): List<Todo> =
        todoRepository.getAll()
}