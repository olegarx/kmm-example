package com.example.kmm.application.features.sample.domain

import com.example.kmm.application.features.sample.domain.model.Todo

interface TodoRepository {

    @Throws(Exception::class)
    suspend fun getAll(): List<Todo>

    @Throws(Exception::class)
    suspend fun get(id: Int): Todo
}