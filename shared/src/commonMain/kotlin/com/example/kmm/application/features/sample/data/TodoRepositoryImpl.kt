package com.example.kmm.application.features.sample.data

import com.example.kmm.application.features.sample.domain.TodoRepository
import com.example.kmm.application.features.sample.domain.model.Todo

class TodoRepositoryImpl(private val todoDataSource: TodoDataSource) : TodoRepository {

    override suspend fun getAll(): List<Todo> =
        todoDataSource.getAll()

    override suspend fun get(id: Int): Todo =
        todoDataSource.get(id)
}