package com.example.kmm.application.features.sample.domain

import com.example.kmm.application.features.sample.domain.model.Todo

class GetTodoUseCase(private val todoRepository: TodoRepository) {

    @Throws(Exception::class)
    suspend operator fun invoke(id: Int): Todo =
        todoRepository.get(id)
}