package com.example.kmm.application.common.coroutines

import kotlin.coroutines.cancellation.CancellationException

/**
 * Для упрощения обработки ошибок
 */

private typealias CallBlock = suspend () -> Unit

fun callBlock(callBlock: CallBlock): CallBlock = callBlock

@Throws(CancellationException::class)
fun CallBlock.onError(action: (Throwable) -> Unit): CallBlock = {
    try {
        this()
    } catch (e: CancellationException) {
        throw e
    } catch (error: Throwable) {
        action(error)
    }
}

suspend fun CallBlock.call() {
    this()
}