package com.example.kmm.application.common.mvvm

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

abstract class BaseViewModel {

    private val supervisorJob = SupervisorJob()

    val viewModelScope: CoroutineScope =
        CoroutineScope(Dispatchers.Main + supervisorJob)

    fun cancel() {
        supervisorJob.cancel()
    }
}