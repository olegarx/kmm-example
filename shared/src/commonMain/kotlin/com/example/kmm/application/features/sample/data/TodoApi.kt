package com.example.kmm.application.features.sample.data

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.kmm.application.features.sample.domain.model.Todo

interface TodoApi {

    @Throws(Exception::class)
    suspend fun get(id: Int): Todo

    @Throws(Exception::class)
    suspend fun getAll(): List<Todo>
}

class TodoApiImpl(private val client: HttpClient) : TodoApi {

    private companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
        const val METHOD_NAME = "todos/"
    }

    override suspend fun get(id: Int): Todo =
        withContext(Dispatchers.Default) {
            println("todos get: Request context: $coroutineContext")
            client.get(BASE_URL + METHOD_NAME + id)
        }

    override suspend fun getAll(): List<Todo> =
        withContext(Dispatchers.Default) {
            println("todos getAll: Request context: $coroutineContext")
            client.get(BASE_URL + METHOD_NAME)
        }
}