package com.example.kmm.application.features.sample.presentation

import com.example.kmm.application.common.flow.CommonFlow
import com.example.kmm.application.common.flow.asCommonFlow
import com.example.kmm.application.common.mvvm.BaseViewModel
import com.example.kmm.application.features.sample.domain.GetTodoListUseCase
import com.example.kmm.application.features.sample.domain.GetTodoUseCase
import com.example.kmm.application.features.sample.domain.model.Todo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import com.example.kmm.application.common.coroutines.call
import com.example.kmm.application.common.coroutines.callBlock
import com.example.kmm.application.common.coroutines.onError

class TodoListViewModel(
	private val getTodoListUseCase: GetTodoListUseCase,
	private val getTodoUseCase: GetTodoUseCase
) : BaseViewModel() {

    private val mutableTodoList = MutableStateFlow<List<Todo>>(emptyList())
    val todoList: CommonFlow<List<Todo>> = mutableTodoList.asCommonFlow()

    private val mutableErrorText = MutableStateFlow("")
    val errorText: CommonFlow<String> = mutableErrorText.asCommonFlow()

    fun updateList() {
        viewModelScope.launch {
            callBlock {
                println("updateList() View model context: $coroutineContext")
                val result = getTodoListUseCase()
                mutableTodoList.value = result
            }.onError {
                println("Error: ${it.message}")
                mutableErrorText.value = "Error ${it.message}"
            }.call()
        }
    }

    fun updateTodo(id: String) {
        viewModelScope.launch {
            callBlock {
                println("updateTodo() View model context: $coroutineContext")
                mutableTodoList.value = listOf(getTodoUseCase(id.toInt()))
            }.onError {
                mutableErrorText.value = "Error ${it.message}"
            }.call()
        }
    }
}