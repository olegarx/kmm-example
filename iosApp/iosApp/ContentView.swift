import SwiftUI
import shared

struct ContentView: View {
    
    let todoApi: TodoApi
    let todoDataSource: TodoDataSource
    let todoRepository: TodoRepository
    let getTodoListUseCase: GetTodoListUseCase
    let getTodoUseCase: GetTodoUseCase
    let viewModel: TodoListViewModel
    
    @State
    var todoListOutput: String = "Loading..."
    
    init() {
        todoApi = TodoApiImpl(client: KtorClientKt.KtorClient)
        todoDataSource = TodoDataSourceImpl(api: todoApi)
        todoRepository = TodoRepositoryImpl(todoDataSource: todoDataSource)
        getTodoListUseCase = GetTodoListUseCase(todoRepository: todoRepository)
        getTodoUseCase = GetTodoUseCase(todoRepository: todoRepository)
        viewModel = TodoListViewModel(getTodoListUseCase: getTodoListUseCase, getTodoUseCase: getTodoUseCase)
    }
    
    func observeViewModel() {
        viewModel.todoList.watch(block: { result in
            guard let list = result?.compactMap({ $0 as? Todo}) else {
                return
            }
            
            self.todoListOutput = list.map({ todo in
                "id = \(todo.id), title = \(todo.title), completed = \(todo.completed)"
            }).joined(separator: "\n")
        })
    }
    
    func load() {
        viewModel.updateList()
    }

	var body: some View {
        ScrollView() {
            Text(todoListOutput)
                .padding(EdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16))
        }
        .onAppear {
            observeViewModel()
            load()
        }
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
